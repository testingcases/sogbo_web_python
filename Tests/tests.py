# tests.py
import time
import unittest
from selenium import webdriver
from config.Config import Config
from ambiente.Ambiente import Ambiente
from actions.Actions import Actions  # Importe a classe corretamente

class TestLogin(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.ambiente_hml = Ambiente.HOMOLOG

    def setUp(self):
        self.driver = self.config.webdriver(self.ambiente_hml)
        self.action = Actions(self.driver)

    def tearDown(self):
        self.driver.quit()

    def test_login_valido(self):
        self.action.acessar_sistema()
        self.action.preencher_usuario("master1")
        self.action.preencher_senha("masterkey")
        self.action.clicar_no_botao_entrar()

        time.sleep(10)

if __name__ == "__main__":
    unittest.main()
