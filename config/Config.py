from selenium import webdriver

class Config:
    def webdriver(self, ambiente):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--remote-allow-origins=*")
        driver = webdriver.Chrome(options=chrome_options)
        #driver.get(ambiente.url)
        driver.get( ambiente.get_url() )

        driver.maximize_window()
        return driver

