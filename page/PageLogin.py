from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class PageLogin:
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 10)

    # Mapeamento dos elementos
    btnAcessarSistema = (By.XPATH, "/html/body/div/div/main/div/div/section[1]/div/footer/button")
    user = (By.NAME, "usuarionome")
    password = (By.NAME, "senha")
    entrar = (By.XPATH, "/html/body/div/div/div/div[2]/form/div[4]/input")

    # Métodos para interações com os elementos
    def click_btn_acessar_sistema(self):
        self.wait.until(EC.element_to_be_clickable(self.btnAcessarSistema)).click()

    def preencher_usuario(self, user):
        self.wait.until(EC.visibility_of_element_located(self.user)).send_keys(user)

    def preencher_senha(self, password):
        self.wait.until(EC.visibility_of_element_located(self.password)).send_keys(password)

    def clicar_no_botao_entrar(self):
        self.wait.until(EC.element_to_be_clickable(self.entrar)).click()
