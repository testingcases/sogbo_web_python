from page.PageLogin import PageLogin


class Actions:
    def __init__(self, driver):
        self.page_login = PageLogin(driver)

    def acessar_sistema(self):
        self.page_login.click_btn_acessar_sistema()

    def preencher_usuario(self, user):

        self.page_login.preencher_usuario(user)

    def preencher_senha(self, password):
        self.page_login.preencher_senha(password)

    def clicar_no_botao_entrar(self):
        self.page_login.clicar_no_botao_entrar()
