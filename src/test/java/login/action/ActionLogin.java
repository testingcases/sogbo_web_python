package login.action;
import login.page.PageLogin;
import org.openqa.selenium.WebDriver;

public class ActionLogin {
    private final PageLogin pageLogin;
    public ActionLogin(WebDriver driver) {
        pageLogin = new PageLogin(driver);
    }

    public void AcessarSistema(){
        pageLogin.btnAcessarSistema.click();

    }

    public void preencherUsuario(String user){
        pageLogin.user.sendKeys(user);

    }
    public void preencherSenha(String password){
        pageLogin.password.sendKeys(password);

    }

    public void clicarNoBotaoEntrar(){
        pageLogin.entrar.click();

    }
}
