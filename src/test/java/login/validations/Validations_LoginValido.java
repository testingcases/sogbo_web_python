package login.validations;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class Validations_LoginValido {
    private WebDriver driver;

    public Validations_LoginValido(WebDriver driver) {
        this.driver = driver;
    }

    public void validarLogin_Sucesso() {
        // Obter a URL atual
        String urlAtual = driver.getCurrentUrl();

        // Definir a URL desejada
        String urlDesejada = "https://ee91-177-74-76-56.ngrok-free.app/modulo";

        // Verificar se a URL atual é igual à URL desejada
        if (urlAtual.equals(urlDesejada)) {
            // Se a URL for igual, exibir mensagem de sucesso
            System.out.println("O teste passou. A URL é: " + urlAtual);
        } else {
            // Se a URL não for igual, forçar a falha do teste e exibir mensagem de erro
            Assert.fail("O teste falhou. A URL não é a esperada. URL atual: " + urlAtual);
        }
    }
}