package login.validations;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class ValidationComAlert {
    private WebDriver driver;

    public ValidationComAlert(WebDriver driver) {
        this.driver = driver;
    }

    public void modalAlert() {
        // Obter a URL atual
        String urlAtual = driver.getCurrentUrl();

        // Definir a URL desejada
        String urlDesejada = "https://ee91-177-74-76-56.ngrok-free.app/modulo";

        // Verificar se a URL atual é igual à URL desejada
        if (urlAtual.equals(urlDesejada)) {
            // Se a URL for igual, exibir alerta verde
            String script = "var div = document.createElement('div');"
                    + "div.style.position = 'fixed';"
                    + "div.style.left = '0';"
                    + "div.style.top = '0';"
                    + "div.style.width = '100%';"
                    + "div.style.padding = '10px';"
                    + "div.style.zIndex = '9999';"
                    + "div.style.textAlign = 'center';"
                    + "div.style.fontWeight = 'bold';"
                    + "div.style.fontSize = '18px';"
                    + "div.style.backgroundColor = 'green';"
                    + "div.innerText = 'Teste passou. A URL é: " + urlAtual + "';";
            ((JavascriptExecutor) driver).executeScript(script);
        } else {
            // Se a URL não for igual, exibir alerta vermelho
            String script = "var div = document.createElement('div');"
                    + "div.style.position = 'fixed';"
                    + "div.style.left = '0';"
                    + "div.style.top = '0';"
                    + "div.style.width = '100%';"
                    + "div.style.padding = '10px';"
                    + "div.style.zIndex = '9999';"
                    + "div.style.textAlign = 'center';"
                    + "div.style.fontWeight = 'bold';"
                    + "div.style.fontSize = '18px';"
                    + "div.style.backgroundColor = 'red';"
                    + "div.innerText = 'Teste falhou. A URL não é a esperada. URL atual: " + urlAtual + "';";
            ((JavascriptExecutor) driver).executeScript(script);

            // Forçar a falha do teste
            Assert.fail("O teste falhou. A URL não é a esperada. URL atual: " + urlAtual);
        }
    }
}
