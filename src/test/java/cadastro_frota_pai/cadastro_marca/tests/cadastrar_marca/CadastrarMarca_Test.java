package cadastro_frota_pai.cadastro_marca.tests.cadastrar_marca;


import cadastro_frota.validations.Scroll;
import cadastro_frota_pai.cadastro_frota.actions.FrotaActions;
import cadastro_frota_pai.cadastro_marca.actions.MarcaActions;
import config.Ambiente;
import config.Config;
import config.User;
import login.action.ActionLogin;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

public class CadastrarMarca_Test {
    private WebDriver driver;
    private User user;
    //String nomeFrota = "Frota Freitas";
    String nomeMarca = "Marca Test";
    String informacoesMarca = "Marca";




    @BeforeClass
    public void criarDriver() {
        Config webdriver = new Config();
        driver = webdriver.webdriver(Ambiente.HOMOLOG);
        user = User.getUser("master1");
    }

    @AfterClass
    public void fecharDriver() {
        // driver.quit();
    }

    @Test(priority = 1, groups = "login")
    public void Dado_acesso_paginaWeb_SOGBOWEB() {
        // Nada a fazer aqui, pois já é feito no @BeforeClass
    }

    @Test(priority = 2, groups = "login", dependsOnMethods = "Dado_acesso_paginaWeb_SOGBOWEB")
    public void Quando_inserir_email_e_senha_Validos() throws InterruptedException {
        ActionLogin actions = new ActionLogin(driver);

        actions.AcessarSistema();
        actions.preencherUsuario(user.getEmail());
        actions.preencherSenha(user.getPassword());
        actions.clicarNoBotaoEntrar();
    }

    @Test(priority = 3, groups = "marca", dependsOnMethods = "Quando_inserir_email_e_senha_Validos")
    public void E_Acesar_O_Modulo_Marca() {
        FrotaActions frota = new FrotaActions(driver);
        MarcaActions marca  = new MarcaActions(driver);

        frota.CaminhoPara_CriarEditarExcluir_Frota();
        marca.ClicarModuloModelo();
        marca.BtnAdicionarModelo();

    }

    @Test(priority = 4, groups = "marca", dependsOnMethods = "E_Acesar_O_Modulo_Marca")
    public void Entao_DeverSerPossivel_CadastrarUmaFrota_ComSucesso() throws InterruptedException {
        FrotaActions frota = new FrotaActions(driver);
        Scroll scrol = new Scroll(driver);


//        frota.InserirInformacoesDaFrota(nomeMarca, informacoesMarca);
        scrol.scrollModalDown(500);
        Thread.sleep(3000);
        driver.findElement(By.xpath("//input[@type='file'][@class='custom-file-input'][@id='imgload'][@name='imgload']")).click();
        frota.ClicarBtnInserirImagem();


//        WebElement fileInput = driver.findElement(By.id("imgload"));
//        fileInput.sendKeys((CharSequence) new File("C:\\Users\\Usuario\\Pictures\\Screenshots\\Captura.png"));
//        fileInput.click();

        //frota.InserirImagem();
        //scrol.scrollModalDown(100);
        //frota.BtnConfirmar_Cadastro_Geral();// Componente Global Extensão cara cadastro de tipo



       // frota.BtnAdicionarFrota(); // Componente Global Extensão cara cadastro de tipo
    }
//
//    @Test(priority = 5, groups = "Frota", dependsOnMethods = "Entao_DeverSerPossivel_CadastrarUmaFrota_ComSucesso")
//    public void Validar_SeFrota_FoiCriada_ComSucesso() throws InterruptedException {
//        FrotaActions frota = new FrotaActions(driver);
//        Validacao_CadastroFrota_Validations validacao = new Validacao_CadastroFrota_Validations(driver);
//
//        frota.PesquisarNomeDaFrota(nomeFrota);
//        Thread.sleep(2000);
//        validacao.validarFrotaCriadaComSucesso(nomeFrota);
    }

//    from playwright.sync_api import sync_playwright
//        import time
//
//        with sync_playwright() as p:
//        navegador = p.chromium.launch(headless= False)
//        pagina = navegador.new_page()
//        pagina.goto("https://www.linkedin.com/notifications/?filter=all")
//        time.sleep(10)


