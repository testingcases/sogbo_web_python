package cadastro_veiculo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class VeiculoPage {
    private final WebDriver driver;

    public VeiculoPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Mapping Criar Frota
    @FindBy(how = How.XPATH, using = "/html/body/div/aside/section/ul/li[1]/ul/li[1]/ul/li[7]/a")
    public WebElement ClicarModuloVeiculo;
    @FindBy(how = How.XPATH, using = "//*[@id=\"btn-create\"]")
    public WebElement btn_AdicionarVeiculo;
}



