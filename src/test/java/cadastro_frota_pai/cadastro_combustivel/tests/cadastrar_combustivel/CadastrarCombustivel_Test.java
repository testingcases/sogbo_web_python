package cadastro_frota_pai.cadastro_combustivel.tests.cadastrar_combustivel;

import cadastro_frota.validations.Scroll;
import cadastro_frota_pai.cadastro_combustivel.actions.CombustivelActions;
import cadastro_frota_pai.cadastro_frota.actions.FrotaActions;
import config.Ambiente;
import config.Config;
import config.User;
import login.action.ActionLogin;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CadastrarCombustivel_Test {

    private WebDriver driver;
    private User user;
    //String nomeFrota = "Frota Freitas";
    String nomeModelo = "Combustivel Test";
    String informacoesModelo = "Descrição Combustivel Teste";


    @BeforeClass
    public void criarDriver() {
        Config webdriver = new Config();
        driver = webdriver.webdriver(Ambiente.HOMOLOG);
        user = User.getUser("master1");
    }

    @AfterClass
    public void fecharDriver() {
        // driver.quit();
    }

    @Test(priority = 1, groups = "login")
    public void Dado_acesso_paginaWeb_SOGBOWEB() {
        // Nada a fazer aqui, pois já é feito no @BeforeClass
    }

    @Test(priority = 2, groups = "login", dependsOnMethods = "Dado_acesso_paginaWeb_SOGBOWEB")
    public void Quando_inserir_email_e_senha_Validos() throws InterruptedException {
        ActionLogin actions = new ActionLogin(driver);

        actions.AcessarSistema();
        actions.preencherUsuario(user.getEmail());
        actions.preencherSenha(user.getPassword());
        actions.clicarNoBotaoEntrar();
    }

    @Test(priority = 3, groups = "marca", dependsOnMethods = "Quando_inserir_email_e_senha_Validos")
    public void E_Acesar_O_Modulo_Marca() {
        FrotaActions frota = new FrotaActions(driver);
        CombustivelActions  combustivel  = new CombustivelActions(driver);

        frota.CaminhoPara_CriarEditarExcluir_Frota();
        combustivel.ClicarModuloCombustivel();
        combustivel.BtnAdicionarCombustivel();

    }

    @Test(priority = 4, groups = "marca", dependsOnMethods = "E_Acesar_O_Modulo_Marca")
    public void Entao_DeverSerPossivel_CadastrarUmaFrota_ComSucesso() {
        FrotaActions frota = new FrotaActions(driver);
        Scroll scrol = new Scroll(driver);

       frota.InserirInformacoesDaFrota(nomeModelo, informacoesModelo);
       scrol.scrollModalDown(100);
       frota.BtnConfirmar_Cadastro_Geral();// Componente Global Extensão cara cadastro de tipo
        frota.BtnAdicionarFrota(); // Componente Global Extensão cara cadastro de tipo
    }



//    @Test(priority = 5, groups = "Frota", dependsOnMethods = "Entao_DeverSerPossivel_CadastrarUmaFrota_ComSucesso")
//    public void Validar_SeFrota_FoiCriada_ComSucesso() throws InterruptedException {
//        FrotaActions frota = new FrotaActions(driver);
//        Validacao_CadastroFrota_Validations validacao = new Validacao_CadastroFrota_Validations(driver);
//
//        frota.PesquisarNomeDaFrota(nomeFrota);
//        Thread.sleep(2000);
//        validacao.validarFrotaCriadaComSucesso(nomeFrota);
}


