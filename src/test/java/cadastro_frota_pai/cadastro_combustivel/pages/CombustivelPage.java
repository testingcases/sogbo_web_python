package cadastro_frota_pai.cadastro_combustivel.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CombustivelPage {
    private final WebDriver driver;

    public CombustivelPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Mapping Criar Frota
    @FindBy(how = How.XPATH, using = "/html/body/div/aside/section/ul/li[1]/ul/li[1]/ul/li[6]/a")
    public WebElement BtnModuloCombustivel;
    @FindBy(how = How.XPATH, using = "//*[@id=\"btn-create\"]/span")
    public WebElement btn_AdicionarCombustivel;
}



