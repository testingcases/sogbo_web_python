package cadastro_frota.validations;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class Scroll {
    private final WebDriver driver;

    public Scroll(WebDriver driver) {
        PageFactory.initElements(driver, this);

        this.driver = driver;

    }
    public void scrollModalDown(int pixelsToScroll) {
        WebElement campoObservacao = driver.findElement(By.id("nome"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].scrollIntoView();", campoObservacao);

        jsExecutor.executeScript("window.scrollBy(0, arguments[0]);", pixelsToScroll);
    }
}

