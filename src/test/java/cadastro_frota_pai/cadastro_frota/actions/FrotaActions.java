package cadastro_frota_pai.cadastro_frota.actions;

import cadastro_frota_pai.cadastro_frota.pages.FrotaPage;
import login.page.PageLogin;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import java.io.File;

public class FrotaActions {
    private final PageLogin pageLogin;
    private final FrotaPage frota;
    // Variável para o upload de imagens
    private File uploadFile;
    private WebDriver driver;


    public FrotaActions(WebDriver driver) {
        pageLogin = new PageLogin(driver);
        frota     = new FrotaPage(driver);
        this.driver = driver;
    }

    //Cadastrar frota...
    public void CaminhoPara_CriarEditarExcluir_Frota(){
        frota.bntCadastrarFrota.click();
        frota.expandirMenuPrincipal.click();
        frota.expandirMenuCadastros.click();
        frota.expandirFrota.click();


    }
    public void BtnAdicionarFrota(){
        frota.clicarNoBotaoFrota.click();
        frota.btnAdicionarFrota.click();
    }

    public void InserirInformacoesDaFrota(String nome,String observacoes ){
        frota.nomeFrota.clear();
        frota.nomeFrota.sendKeys(nome);
        frota.observacoes.clear();
        frota.observacoes.sendKeys(observacoes);
    }
    public void ClicarBtnInserirImagem(){
        frota.imagem.click();
    }


//    public void InserirImagem(String caminhoImagem) {
//        WebElement inputImagem = driver.findElement(By.id("C:\\Users\\Usuario\\Pictures\\Screenshots\\Captura de tela 2023-11-11 215725.png"));
//
//        // Insere o caminho do arquivo diretamente no campo de entrada
//
//        inputImagem.sendKeys(caminhoImagem);
//
//        // Envia o formulário
//    }
//        public void InserirImagem(File uploadFile) {
//            WebElement fileInput = driver.findElement(By.xpath("//*[@id=\"imgshow\"]"));
//            fileInput.sendKeys("C:\\Users\\Usuario\\Pictures\\Screenshots\\Captura de tela de 2023-12-09 20-57-25.png");
////            fileInput.sendKeys(uploadFile.getAbsolutePath());
////            driver.findElement(By.xpath("file-submit")).click();
//        }
//    public void InserirImagem(String caminhoImagem) {
//        WebElement inputImagem = driver.findElement(By.id("imgshow"));
//
//        // Seleciona o arquivo a ser inserido
//        File file = new File(caminhoImagem);
//        inputImagem.sendKeys(file.getAbsolutePath());
//
//        // Envia o formulário
//
//    }


        public void BtnConfirmar_FrotaCriada_ComSucesso(){
        frota.btnConfirmarCadsatroFrota.click();
    }

    public void BtnConfirmar_Cadastro_Geral(){
        frota.btnConfirmarCadastroGeral.click();
    }

    public void BtnConfirmar_Editada_ComSucesso(){
        frota.btnConfirmarEdicaoFrota.click();
    }


    //Editar frota...
    public void ClicarNoBotaoEditar(){
        frota.ClicarNoBotaoEditar.click();
    }


    //Exlcuir frota...
    public void PesquisarNomeDaFrota(String Editarnome ){
        frota.EditarNomeFrota.sendKeys(Editarnome);
    }
    public void ClicarNoBotaoExcluir( ){
        frota.btnExcluir.click();
    }
    public void ClicarNoBotaoConfirmarExclusao( ){
        frota.confirmarExclusao.click();
    }
}


