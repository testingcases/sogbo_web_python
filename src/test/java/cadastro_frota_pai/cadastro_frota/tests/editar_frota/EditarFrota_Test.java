package cadastro_frota_pai.cadastro_frota.tests.editar_frota;

import cadastro_frota_pai.cadastro_frota.actions.FrotaActions;
import config.Ambiente;
import config.Config;
import config.User;
import cadastro_frota.validations.Scroll;
import login.action.ActionLogin;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EditarFrota_Test {
        private WebDriver driver;
        private User user;
        //String nome = "Frota Batist ";
        String nome = "San Pieerre LTDA";
        String informacoesEdicao = "Teste Edicao ";
       String nomeFrotaEdicao = "Frota Ediçao Teste";

        @BeforeClass
        public void criarDriver() {
            Config webdriver = new Config();
            driver = webdriver.webdriver(Ambiente.HOMOLOG);
            user = User.getUser("master1");
        }

        @AfterClass
        public void fecharDriver() {
            // driver.quit();
        }


        @Test(priority = 1, groups = "login")
        public void Dado_acesso_paginaWeb_SOGBOWEB() {
            // Nada a fazer aqui, pois já é feito no @BeforeClass
        }

        @Test(priority = 2, groups = "login", dependsOnMethods = "Dado_acesso_paginaWeb_SOGBOWEB")
        public void Quando_inserir_email_e_senha_Validos() throws InterruptedException {
            ActionLogin actions = new ActionLogin(driver);

            actions.AcessarSistema();
            actions.preencherUsuario(user.getEmail());
            actions.preencherSenha(user.getPassword());
            actions.clicarNoBotaoEntrar();

        }

        @Test(priority = 3, groups = "Frota", dependsOnMethods = "Quando_inserir_email_e_senha_Validos")
        public void E_Acesar_O_Modulo_Frota() {
            FrotaActions frota = new FrotaActions(driver);

            frota.CaminhoPara_CriarEditarExcluir_Frota();

        }

        @Test(priority = 4, groups = "Frota", dependsOnMethods = "E_Acesar_O_Modulo_Frota")
        public void Entao_DeverSerPossivel_ExcluirUmaFrota_ComSucesso() throws InterruptedException {
            FrotaActions frota = new FrotaActions(driver);
            Scroll scrol = new Scroll(driver);

            frota.PesquisarNomeDaFrota(nome);
            Thread.sleep(2000);
            frota.ClicarNoBotaoEditar();
            frota.InserirInformacoesDaFrota(nomeFrotaEdicao, informacoesEdicao);
            scrol.scrollModalDown(100);
            frota.BtnConfirmar_Editada_ComSucesso();


        }
    }


