package config;

public enum Ambiente {
    LOCAL(" "),

  HOMOLOG("https://6d5d-177-74-78-220.ngrok-free.app"),
    PROD(" ");

    private final String url;

    Ambiente(String url){
        this.url = url;
    }

    public String getUrl(){
        return this.url;
    }
}

