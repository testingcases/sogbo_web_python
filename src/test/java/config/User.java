package config;

import java.util.HashMap;
import java.util.Map;

public class User {
    private String username;
    private String email;
    private String password;

    // Mapear os usuários e suas informações (email e senha)
    private static final Map<String, User> users = new HashMap<>();

    static {
        // Adicionar os usuários com seus emails e senhas
        users.put("master1", new User ("master1", "masterkey"));

    }

    private User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public static User getUser(String username) {
        return users.get(username);
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
