from enum import Enum

class Ambiente(Enum):
    LOCAL = " "
    HOMOLOG = "https://f679-177-74-78-220.ngrok-free.app/"

    PROD = " "

    def get_url(self):
        return self.value
